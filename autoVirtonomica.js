// ==UserScript==
// @name           autoVirtonomica
// @description    Maximum Virtonomica automatization
// @version        1
// @author		   AiSee
// @require        https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js
// @include        https://virtonomica.ru/*
// ==/UserScript==

function avRun() {
	var l = console.log;
	var s = localStorage;

	//function getType()
	//возвращает тип в виде строки (по изображению)
	function getType() {
		var img = $("#unitImage").html();
		if (img === undefined)
			return 'unknown';

		img = img.substr(0, img.length - 8);
		var txt = img.match(/\w+$/)[0];
		switch (txt) {
			case('service_light'):
				return 'service';
			case('power'):
				return 'energy';
			default:
				return txt;
		}
	}

	function getTechForKval(kval) {
		return Math.pow(kval, 0.8);
	}

	// возвращает квалификацию игрока
	function getPlayerQual() {
		return parseInt($('a:contains("Квалификация игрока")').parent().next().text());
	}

	// q - квалификация игрока
	//вычисляет максимальное кол-во работающих на предприятиях отрасли для заданной квалификации игрока (топ-3)
	function calcPersonalTop3(q, type) {
		return ((2 * q * q + 6 * q) * getK3(type));
	}

	// q - квалификация игрока
	// qp - квалификация персонала
	// вычисляет максимальное кол-во работающих с заданной квалификацией на предприятии для заданной квалификации игрока (топ-1)
	function calcPersonalTop1(q, qp, type) {
		return Math.floor(0.2 * getK1(type) * 14 * q * q / Math.pow(1.4, qp));
	}

	// q - квалификация игрока
	// p - численность персонала
	// вычисляет максимальное квалификацию работающих при заданных их численности и квалификации игрока (обратна calcPersonalTop1())
	function calcQualTop1(q, p, type) {
		if (p === 0) return 0.00;
		return Math.log(0.2 * 14 * getK1(type) * q * q / p) / Math.log(1.4);
	}

	// q - квалификация игрока
	// вычисляет максимальный уровень технологии для заданной квалификации игрока
	function calcTechMax(q) {
		return Math.floor(10 * Math.pow(q / 0.0064, 1 / 3)) / 10;
	}

	// qp - квалификация игрока
	// вычисляет максимальное качество оборудования/животных для заданной квалификации персонала
	function calcEqQualMax(qp) {
		return Math.floor(100 * Math.pow(qp, 1.5)) / 100;
	}

	//function getPersonalTotal()
	//возвращает суммарное кол-во работающих в отрасли (топ-3)
	function getPersonalTotal(new_interface) {
		if (new_interface) {
			return parseInt($("#all_staff").text().replace(/\s+/g, ''));
		} else {
			return parseInt($('td.title:contains("Суммарное")').next().text().replace(' ', '').replace(' ', '').replace(' ', '').replace(' ', '').replace(' ', '').replace(' ', '').replace(' ', '').replace(' ', ''));
		}

	}

	//getK1(type)
	//возвращает к для расчётов нагрузки по типу для топ-1
	function getK1(type) {
		switch (type) {
			case('shop'):
			case('restaurant'):
			case('lab'):
				return 5;
			case('workshop'):
				if (/anna/.test(window.location.href))
					return 100;
				return 50;
			case('mill'):
				return 5;
			case('sawmill'):
				return 12.5;
			case('animalfarm'):
				return 7.5;
			case('medicine'):
			case('fishingbase'):
				return 12.5;
			case('farm'):
				return 20;
			case('orchard'):
				return 15;
			case('mine'):
				if (/anna/.test(window.location.href))
					return 50;
				return 100;
			case('office'):
			case('it'):
				return 1;
			case('service'):
				return 1.5;
			case('energy'):
				return 75.0;
			case('repair'):
			case('fuel'):
				return 2.5;
			case('educational'):
				return 1.5;
			case('villa'):
			case('warehouse'):
			case('unknown'):
			default:
				return 0;
		}
	}

	//возвращает к для расчётов нагрузки по типу для топ-3
	function getK3(type) {
		switch (type) {
			case('shop'):
			case('restaurant'):
			case('lab'):
				return 5;
			case('workshop'):
			case('mill'):
			case('sawmill'):
				if (/anna/.test(window.location.href))
					return 100;
				return 50;
			case('animalfarm'):
				return 7.5;
			case('medicine'):
			case('fishingbase'):
				return 12.5;
			case('farm'):
			case('orchard'):
				return 20;
			case('mine'):
				if (/anna/.test(window.location.href))
					return 50;
				return 100;
			case('energy'):
				return 75.0;
			case('repair'):
			case('fuel'):
				return 2.5;
			case('service'):
			case('educational'):
				return 1.5;
			case('office'):
			case('it'):
				return 1;
			case('villa'):
			case('warehouse'):
			case('unknown'):
			default:
				return 0;
		}
	}

	function getPersonalBlock(type) {
		switch (type) {
			case('lab'):
				return 'Количество учёных';
			case('workshop'):
			case('mill'):
			case('mine'):
			case('fishingbase'):
			case('sawmill'):
			case('energy'):
				return 'Количество рабочих';
			case('animalfarm'):
			case('orchard'):
			case('farm'):
				return 'Количество раб';
			case('medicine'):
			case('office'):
			case('shop'):
			case('restaurant'):
			case('service'):
			case('repair'):
			case('fuel'):
			case('educational'):
			case('it'):
				return 'Количество сотрудников';
			case('villa'):
			case('warehouse'):
			case('unknown'):
			default:
				return -1; // error
		}
	}

	//возвращает кол-во работников на предприятии (по типу)
	function getPersonal(type) {
		return parseInt($('td:contains(' + getPersonalBlock(type) + ')').next().html().replace(/\s+/g, ''));
	}

	function getPersonalNeed(type) {
		var m = $('td:contains(' + getPersonalBlock(type) + ')').next().text().match(/\(\D+(\d+)\)/);
		if (m !== null)
			return toNum(m[1]);
		else
			return getPersonal(type);
	}

	// возвращает аргумент округлённым до 2-го знака
	function f2(val) {
		return Math.floor(100 * val) / 100;
	}

	function kvalaPlugin() {
		var style = document.createElement("style");
		style.textContent = '.scriptIks_imp { border: 2px solid #708090; border-radius: 5px; background: #e1e1e1; text-align: right; }'
			+ '\n.scriptIks_cur { cursor: pointer; }'
			+ '\n.scriptIks_exitDiv { position: absolute; margin: 0; padding: 0; right: 0; top: 0; }'
			+ '\n.scriptIks_crug { position: absolute; right: 0; top: 0; width: 14px; height: 14px; border: 4px solid gray; border-radius: 50%; padding: 0; }'
			+ '\n.scriptIks_crug1 { position: absolute; right: 0; top: 0; width: 14px; height: 14px; border: 4px solid gray; border-radius: 0 50% 0 50%; padding: 0;  background:white; }'
			+ '\n.scriptIks_exit { position: absolute; margin: 0; padding: 0; right: 5px; top: 1px; font-size: 18px; color: darkred; }'
			+ '\n.scriptIks_but { color:white; border:1px solid #708090; border-radius: 10px; background: #708090; background: linear-gradient(top, #e1e1e1, #708090, #e1e1e1); background: -webkit-linear-gradient(top, #e1e1e1, #708090, #e1e1e1); background: -moz-linear-gradient(top, #e1e1e1, #708090, #e1e1e1); background: -ms-linear-gradient(top, #e1e1e1, #708090, #e1e1e1); background: -o-linear-gradient(top, #e1e1e1, #708090, #e1e1e1); }';
		document.documentElement.appendChild(style);

		// возвращает аргумент с разделением пробелами тысяч
		function getThousandsSplitted(val) {
			return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
		}

		// удаляет из строки все символы, кроме цифр и точек
		function leaveOnlyDigitsAndDots(str) {
			return str.replace(/[^\d.]/g, '');
		}

		// возвращает процент от val по отношению к nun
		function procVal(num, val) {
			return Math.round(val / (num / 100) * 100) / 100;
		}

		var new_interface = $("div.unit_box-container").length;
		if (new_interface) {
			// поменять местами заметки и инфу по топ-менеджеру
			var $notice = $("#notice_div");
			var topBox = $notice.parent().next();
			$notice.parent().insertAfter(topBox);

			// создать кнопку удаления заметки
			var str = '<div class="unit_button btn-virtonomics-unit-cancel" onclick="$(\'#notice_div\').text(\'\');return unitNoticeSet();">';
			str += '<i class="fa fa-trash fa-lg fa-fw"></i>&nbsp; Удалить заметку</div>';
			$notice.next().children().eq(0).children().eq(0).after($(str));
		} else {
			// Процент зарплаты
			$("td:contains('Зарплата')").next().each(function () {
				str = this.innerHTML;

				var zarp = str.match(/\d[.\s\d]*/g);
				var salary = zarp[0].replace(/[^\d.]/g, '');
				var avgSalary = zarp[1].replace(/[^\d.]/g, '');
				var zzz = (salary / avgSalary * 100).toFixed(0);
				$('body').data("avg-salary", avgSalary).data("salary", salary);
				var color = zzz < 80 ? 'red' : 'green';
				color = zzz > 80 ? 'blue' : color;

				this.innerHTML = '<font color=' + color + '>' + str + ' --> ' + zzz + '%</font>';
			});
		}

		// <<< Калькулятор
		$('#unit_subtab').each(function () {
			var str = this.innerHTML;
			str += '<br><div id="calcTop"><a id="calcToBloc" class="popup scriptIks_cur"><u>Калькулятор топ-1</u></a></div>';
			var d = document.createElement('div');
			d.id = 'calcTop1';
			d.style = 'position: fixed; right: 5px; top: 5px; background:gray; padding: 0px;'
				+ ' border-radius:10px; -webkit-border-radius:10px; -moz-border-radius:10px; -khtml-border-radius:10px;';

			var ua = navigator.userAgent;
			var strToBrauzer = '<div style="position:relative; margin: 0; padding: 4px;">';
			if (ua.search(/Chrome/) > 0) strToBrauzer = '<div style="position:absolute; margin: 0; padding: 4px; right: 5px; top: 5px;">';
			d.innerHTML = strToBrauzer
				+ '<table style="background-color: white; border-radius:7px; -webkit-border-radius:7px; -moz-border-radius:7px;">'
				+ '<tr><td align="center" colspan="2" style="color: #708090;"><b><h1>Калькулятор</h1><b><hr></td></tr>'
				+ '<tr><td>Квалификация ТОПа</td> <td><input id="calcTopKv" type="text" size="4" class="scriptIks_imp"></td></tr>'
				+ '<tr><td>Технология</td> <td><input id="calcTopTehImp" type="text" size="4" class="scriptIks_imp"></td></tr>'
				+ '<tr><td>Количество работников</td> <td><input id="calcTopKolRab" type="text" size="4" class="scriptIks_imp"></td></tr>'
				+ '<tr><td>Квалификация работников</td> <td><input id="calcTopKvRab" type="text" size="4" class="scriptIks_imp"></td></tr>'
				+ '<tr><td align="center" colspan="2"><input id="calcButton" type="button" value="Расчитать" class="scriptIks_cur scriptIks_but"></td></tr>'
				+ '<tr><td align="center" colspan="2"><hr></td></tr>'
				+ '<tr><td>Максимальная технология<br>по данной квалификации</td> <td id="calcTopTeh" style="text-align: right;"></td></tr>'
				+ '<tr><td align="center" colspan="2"><hr></td></tr>'
				+ '<tr><td>Максимальное количество<br>персонала при<br>данной квалификации</td> <td id="calcTopRabMax" style="text-align: right;"></td></tr>'
				+ '<tr><td><input id="calcTopRab_MaxImp" value="120" type="text" size="4" class="scriptIks_imp" title="Укажите процент на какой расчитать">&nbsp;%</td> <td id="calcTopRab_Max" style="text-align: right;"></td></tr>'
				+ '<tr><td align="center" colspan="2"><hr></td></tr>'
				+ '<tr><td>Максимальная квалификация<br>персонала при данном количестве<hr></td> <td id="calcTopRab" style="text-align: right;"></td></tr>'
				+ '<tr><td>Минимальная квалификация<br>по данной технолигии</td> <td id="calcTopRabTeh" style="text-align: right;"></td></tr>'
				+ '<tr><td align="center" colspan="2"><hr></td></tr>'
				+ '<tr><td>Максимальное качество<br>оборудования при данной<br>квалификации персонала<hr></td> <td id="calcTopOb" style="text-align: right;"></td></tr>'
				+ '<tr><td>Качество оборудования<br>по данной технолигии</td> <td id="calcTopObTeh" style="text-align: right;"></td></tr>'
				+ '<tr><td align="center" colspan="2"><hr></td></tr>'
				+ '<tr><td>Максимальное количество<br>персонала в отрасли</td> <td id="calcTop3" style="text-align: right;"></td></tr>'
				+ '<tr><td align="center" colspan="2"><hr></td></tr>'
				+ '<tr><td align="right" colspan="2"><a href="http://virtacalc.freehost96.ru/indextop.php" class="popup" title="Так-же расчет нагрузки ТОП-1 и многое другое..." target="_blank"><small>Калькулятор от DeMonyan</small></a></td></tr>'
				+ '<table>'
				+ '<div class="scriptIks_cur scriptIks_exitDiv" id="calcExitBloc" title="Закрыть">'
				+ '<div class="scriptIks_crug1"></div>'
				+ '<div class="scriptIks_exit"><b>X</b></div>'
				+ '<div class="scriptIks_crug"></div>'
				+ '</div>'
				+ '</div>';
			document.body.appendChild(d);

			this.innerHTML = str;
			document.getElementById('calcTop1').style.display = "none";
		});

		document.getElementById('calcButton').onclick = calcTopGet;
		function calcTopGet() {
			var p = false;
			var $ctt = $('#calcTopTeh');
			if ($ctt.html() !== '')
				p = true;
			// Максимум рабов ТОП-3
			kv = $('#calcTopKv').val();
			var pers = calcPersonalTop3(kv, type);
			$('#calcTop3').html(pers);
			// Максимальная техна
			if (p) {
				techn = calcTechMax(kv);
				$ctt.html(Math.floor(techn));
			}
			// Максимальное кол. рабов ТОП-1
			kvp = $('#calcTopKvRab').val();
			emp_count = calcPersonalTop1(kv, kvp, type);
			$('#calcTopRabMax').html(emp_count);
			var kvpTeh = getTechForKval($('#calcTopTehImp').val());
			$('#calcTopRabTeh').html(kvpTeh);
			//-----
			var maxRab = $('#calcTopRab_MaxImp').val();
			$('#calcTopRab_Max').html(Math.floor(emp_count / 100 * maxRab));
			// Макс. квала рабов
			cur_pers = $('#calcTopKolRab').val();
			var maxq = f2(calcQualTop1(kv, cur_pers, type));
			$('#calcTopRab').html(maxq);
			// Макс. оборудование
			max_eq = calcEqQualMax(kvp);
			$('#calcTopOb').html(max_eq);
			if (p) {
				var teh_eq = calcEqQualMax(kvpTeh);
				$('#calcTopObTeh').html(teh_eq);
			} else $('#calcTopObTeh').html('');
		}

		$('#calcToBloc').click(function () {
			document.getElementById('calcTop').style.display = "none";
			document.getElementById('calcTop1').style.display = "block";
		});
		$('#calcExitBloc').click(function () {
			document.getElementById('calcTop').style.display = "block";
			document.getElementById('calcTop1').style.display = "none";
		});
		// >>>

		var type = getType();
		var total = getPersonalTotal(new_interface);
		var cur_pers = getPersonal(type);
		var kv = getPlayerQual();
		var pers = calcPersonalTop3(kv, type);
		$('#calcTopKv').val(kv);

		// топ-3
		$("td:contains('Суммарное')").next().each(function () {
			str = this.innerHTML;
			var pers = calcPersonalTop3(kv, type);
			$('#calcTop3').html(pers);
			var pers_next = calcPersonalTop3(kv + 1, type);
			var overload = procVal(pers, total);

			var font = 'green';
			if (overload > 102)
				font = 'red';
			else if (overload > 100)
				font = 'blue';

			if (new_interface) {
				// выделить цветом число подчиненных
				var curText = $(this).text();
				$(this).html($('<font color=' + font + '><b>' + curText + '</b></font>'));

				var top3AndFree = '<tr><td>Нагрузка топ-3</td>';
				top3AndFree += '<td><font color=' + font + '><b>' + overload + '%</b></font></td></tr>';
				if (overload !== 100) {
					var word = overload < 100 ? 'Свободно' : 'Лишних';
					var delta = Math.abs(pers - total);
					top3AndFree += '<tr><td>' + word + ' подчинённых</td>';
					top3AndFree += '<td><font color=' + font + '><b>' + delta + '</b></font></td></tr>';
				}
				$(this).parent().before($(top3AndFree));

				var str = '<tr><td>Максимальное количество</td><td>' + getThousandsSplitted(pers) + '</td></tr>';
				str += '<tr><td><font color=gray>На следующем уровне квалификации</font></td>';
				str += '<td><font color=gray>' + getThousandsSplitted(pers_next) + '</font></td></tr>';
				$(this).parent().after($(str));
			} else {
				str = ' <br><font color=' + font + '>Предельная нагрузка по квале: <b>';
				str += pers + '</b>' + ' (на след.уровне: ' + '<b>' + pers_next + '</b>' + ')';
				str += '<br>Загрузка топ-3: <b> ' + overload + ' %</b>';
				if (overload < 100) str += '<br>Свободно рабов: <b>' + (pers - total) + '</b>';
				else if (overload > 100) str += '<br>Перебор рабов: <b>' + (total - pers) + '</b>';
				str += '</b></font>';
				this.innerHTML = this.innerHTML + str;
			}
		});

		// Количество работников
		var nofp = getPersonal(type);
		var emp_count = 0;

		// топ-1
		var kvp = 0;
		$("td:contains('Уровень квалификации')").next().each(function () {
			str = this.innerHTML;
			kvp = parseFloat(str);
			$('#calcTopKvRab').val(kvp);
			$('body').data("kv", kvp);
			var kvalNeed = str.match(/требуется\D+([\d.]+)/);
			if (kvalNeed !== null)
				$('body').data("qual-req", kvalNeed[1]);

			emp_count = calcPersonalTop1(kv, kvp, type);
			$('#calcTopRabMax').html(emp_count);
			$('#calcTopKolRab').val(cur_pers);
			var emp_count_next = calcPersonalTop1(kv + 1, kvp, type);

			var maxq = f2(calcQualTop1(kv, cur_pers, type));
			$('#calcTopRab').html(maxq);

			var font = 'green';
			if (maxq < kvp)
				font = 'red';

			// процент загрузки по топ-1
			num = procVal(emp_count, nofp);
			if (num > 100) {
				if (num <= 120) font = 'orange';
				else font = 'red';
			} else font = 'green';

			if (new_interface) {
				// передвинуть уровень загрузки производства туда, где ему и место
				if ($("td:contains('Уровень загрузки производства')").length) {
					$($(this).parent().next().next()).insertBefore($(this).parent().prev().prev());
				}

				// выделить цветом значение квалификации
				var curText = $(this).text();
				$(this).html($('<font color=' + font + '><b>' + curText + '</b></font>'));

				var beforeString = '';
				var avgSalaryStr = null;
				$("td:contains('В среднем по городу'):first").next().each(function () {
					avgSalaryStr = parseFloat($(this).text().replace(/[^\d.]/g, ''));
				});
				if (avgSalaryStr !== null) {
					var curSalary = parseFloat(leaveOnlyDigitsAndDots($(this).parent().prev().prev().children().eq(1).text()));
					$('body').data("avg-salary", avgSalaryStr).data("salary", curSalary);
					var zzzn = (curSalary / avgSalaryStr * 100).toFixed(0);
					var colorn = zzzn < 80 ? 'red' : 'green';
					colorn = zzzn > 80 ? 'blue' : colorn;
					var oldHtml = $(this).parent().prev().prev().children().eq(1).html();
					var colored = '<font color=' + colorn + '>' + oldHtml + '</font>';
					$(this).parent().prev().prev().children().eq(1).html(colored);
					beforeString += '<tr><td>Расходы на зарплату</td>';
					beforeString += '<td>$' + getThousandsSplitted((curSalary * cur_pers).toFixed(2)) + ' в неделю</td></tr>';
				}
				if (isFinite(num)) {
					beforeString += '<tr><td>Нагрузка топ-1</td><td><font color=' + font + '><b>' + num + '%</b></font></td></tr>';
				}
				$(this).parent().before($(beforeString));

				var str = '<tr><td>Квалификация для 100%/120% топ-1</td>';
				str += '<td>' + maxq + '/' + f2(calcQualTop1(kv, cur_pers / 1.2, type)) + '</td></tr>';
				str += '<tr><td>Макс. количество с квалой ' + kvp + '</td>';
				str += '<td>' + emp_count + '</td></tr>';
				str += '<tr><td><font color=gray>120% максимального количества</font></td>';
				str += '<td><font color=gray>' + Math.floor(emp_count / 100 * 120) + '</font></td></tr>';
				str += '<tr><td><font color=gray>Макс. на следующем уровне квалы</font></td>';
				str += '<td><font color=gray>' + emp_count_next + '</font></td></tr>';
				$('#calcTopRab_Max').html(Math.floor(emp_count / 100 * 120));
				$(this).parent().next().after($(str));
			} else {
				this.innerHTML = this.innerHTML + '<br><font color=gray>Максимальная квала для 100% по топ-1: <b>' + maxq + '</b></font>';
				this.innerHTML = this.innerHTML + '<br>(<font color=' + font + '>Максимальное количество рабов: <b>' + (emp_count) + '</b></font>) ';
				this.innerHTML = this.innerHTML + '<br>(<font color=' + font + '>120% количества рабов: <b>' + Math.floor(emp_count / 100 * 120) + '</b></font>)';
				$('#calcTopRab_Max').html(Math.floor(emp_count / 100 * 120));
				// процент загрузки по топ-1
				var num = procVal(emp_count, nofp);
				if (num > 100) {
					if (num <= 120) font = 'orange';
					else font = 'red';
				} else font = 'green';
				this.innerHTML = this.innerHTML + '<br><font color=' + font + '>Нагрузка по топ-1: <b>' + num + '%</b></font>';
				this.innerHTML = this.innerHTML + '<br>(<font color=gray>На квале ' + (kv + 1) + ' (следующий уровень): <b>' + (emp_count_next) + '</b></font>)';

				var p5 = Math.floor(pers / 5);
				var k5 = calcQualTop1(kv, p5, type);
				var e5 = calcEqQualMax(k5);
				this.innerHTML = this.innerHTML + '<br>(<font color=olive>Special 05: <b>5 *' + p5 + '--' + f2(k5) + '(' + e5 + ')' + '</b></font>)';

				p5 = Math.floor(pers / 10);
				k5 = calcQualTop1(kv, p5, type);
				e5 = calcEqQualMax(k5);
				this.innerHTML = this.innerHTML + '<br>(<font color=olive>Special 10: <b>10*' + p5 + '--' + f2(k5) + '(' + e5 + ')' + '</b></font>)';
			}
		});

		var techn = 0;
		var max_techn = 0;
		$("td:contains('Уровень технологии')").next().each(function () {
			str = this.innerHTML;
			techn = parseInt(str);
			$('#calcTopTehImp').val(techn);
			max_techn = calcTechMax(kv);
			$('#calcTopTeh').html(Math.floor(max_techn));
			var font = 'green';
			if (max_techn < techn) {
				font = 'red';
			}
			if (new_interface) {
				var curText = $(this).text();
				$(this).html($('<font color=' + font + '><b>' + curText + '</b></font>'));
				str = '<tr><td>Максимальная технология</td><td>' + (max_techn) + '</td></tr>';
				$(this).parent().after($(str));
			} else {
				str = ' <br>(<font color=' + font + '>Максимальная технология: <b>' + (max_techn) + '</b></font>)';
				this.innerHTML = this.innerHTML + str;
			}
		});

		//оборудование
		var eq;
		var max_eq = 0;
		$("td:contains('Качество '):first").next().each(function () {
			str = this.innerHTML;
			eq = parseFloat(str);
			max_eq = calcEqQualMax(kvp);
			$('#calcTopOb').html(max_eq);

			var font = 'green';
			if (eq > max_eq) {
				font = 'red';
				$('body').data('qual-fix', 1)
			}

			if (new_interface) {
				var required = parseFloat($(this).parent().next().children().eq(1).text());
				if (eq < required) {
					font = 'red';
				}
				// выделить цветом качество
				var curText = $(this).text();
				$(this).html($('<font color=' + font + '><b>' + curText + '</b></font>'));

				str = '<tr><td>Максимальное по персоналу</td><td>' + (max_eq) + '</td></tr>';
				$(this).parent().next().after($(str));
			} else {
				str = ' <br>(<font color=' + font + '>Макс.качество по персоналу: <b>' + (max_eq) + '</b></font>)';
				if (!$("td:contains('Качество животных')")) {
					required = $(this)['0'].innerHTML.match(/\d+\.\d+/g)['1'];
					if (eq < required) {
						font = 'red';
					}
					// выделить цветом качество
					curText = $(this)['0'].innerHTML.match(/\d+\.\d+/g)['0'];
					$(this).html($('<font color=' + font + '><b>' + curText + '</b></font><span> (требуется по технологии ' + required + ')</span>'));
				}
				this.innerHTML = this.innerHTML + str;
			}

			// Вызов калькулятора, чтоб посчтитал
			calcTopGet();
		});

		var d = 1;
		var ob, per;

		// максмальное количество поситетилей по персоналу
		if (type === 'restaurant' || type === 'service' || type === 'medicine' || type === 'repair' || type === 'educational')
			$("td.title:contains('Количество посетителей')").next().each(function () {
				var str = this.innerHTML;

				per = str.match(/\d[.\s\d]*/g);
				per = [per[0].replace(/[^\d.]/g, ''), per[1].replace(/[^\d.]/g, '')];
				var pos = per[0];
				var $body = $('body');
				$body.data("visitors", pos);

				ob = $("td:contains('Количество оборудования')").next().text().replace(' ', '');
				ob = ob.match(/\d[.\s\d]*/g);
				ob = [ob[0].replace(/[^\d.]/g, ''), ob[1].replace(/[^\d.]/g, '')];

				var posMax = per[1];
				var perMax = ob[1];

				var spec = $("td.title:contains('Специализация')").next().text();
				switch (spec) {
					case('Фитнес'):
					case('Йога'):
					case('Бодибилдинг'):
					case('Группы здоровья'):
					case('Профессиональный спорт'):
					case('Скалолазание'):
						d = 5;
						break;
					case('Танцы'):
						d = 5;
						break;
					case('Прачечная'):
					case('Химчистка'):
					case('Прачечная самообслуживания'):
						d = 10;
						break;
					case('Косметический салон'):
					case('Парикмахерская'):
					case('Центр народной медицины'):
					case('Стоматологическая клиника'):
					case('Диагностический центр'):
					case('Поликлиника'):
					case('Больница'):
					case('Кардиологическая клиника'):
					case('Группы подготовки к школе'):
					case('Детский сад'):
					case('Ясли'):
					case('Студия детского творчества'):
					default:
						d = 1;
				}
				perMax = perMax / d;

				var persKol = parseInt($("td:contains('Количество сотрудников')").next().text().replace(' ', ''));
				var maxPer = persKol * (posMax / perMax);
				var s = '<br>Макс. по персоналу: ' + maxPer;
				$body.data("visitors-max", maxPer);

				s += '<br>Посещаемость: ' + ( procVal(maxPer, pos) ) + '%';

				this.innerHTML = str + s;
			});
	}

	// Общие функции
	function toNum(variable) {
		return parseFloat(String(variable).replace(/[^\d.]+/g, "")) || 0;
	}

	function sleep(ms) {
		return new Promise(resolve => setTimeout(resolve, ms));
	}

	async function waitAndDo(ms, item, callback) {
		while (s.getItem(item) < 1) {
			await sleep(ms);
		}
		s.setItem(item, 0);
		callback();
	}

	// Множитель, который может использоваться для учёта занятой товаром доли рынка
	function getShareMul(share) {
		return 1; // + share / 400; // Math.pow(share, 2) / 20000;
	}

	// <<< Unit list
	if (/main\/company\/view\/\d+(\/unit_list)?$/.test(window.location.href)) {
		// Все предприятия для которых есть обработчики получат по кнопке
		var count = 0;
		var $tds = $('td.i-shop, td.i-fuel, td.i-workshop, td.i-kindergarten, td.i-restaurant, ' +
			'td.i-repair, td.i-medicine, td.i-office, td.i-hairdressing, td.i-fitness, td.i-laundry, ' +
			'td.i-cellular, td.i-mill, td.i-animalfarm, td.i-lab'); // , td.i-mine
		$tds.each(function () {
			var $td = $(this).siblings("td.unit_id");
			// var id = $td.text();
			var $a = $(this).find("a");
			if ($a.text().match(/тест|продать/i) === null) {
				var link = $a.prop("href");
				$a.addClass("allAuto").attr("data-count", count);
				$td
					.append(`<br><a class="auto" href="` + link + `" target="_blank">auto</a> `)
					.append(`<a class="all" href="` + link + `" target="_blank" data-count="` + count + `">all</a>`);
				count++;
			}
		});
		$("a.auto").click(function () {
			s.setItem("auto-unit-view-do", "auto");
			s.setItem("auto-unit-view-done", 0);
			s.setItem("auto-unit-view-ids", "[]");
		});
		$("a.all").click(function () {
			var hrefs = [];
			var count = $(this).attr("data-count");
			$("a.allAuto").each(function () {
				if (toNum($(this).attr("data-count")) > toNum(count)) {
					hrefs.push($(this).prop("href"));
				}
			});
			s.setItem("auto-unit-view-do", "auto");
			s.setItem("auto-unit-view-done", 0);
			s.setItem("auto-unit-view-hrefs", JSON.stringify(hrefs));
		});
	}

	// <<< Unit view
	if (/main\/unit\/view\/\d+$/.test(window.location.href)) {
		// Куча кода из другого плагина
		kvalaPlugin();

		function personnel(type) {
			var $body = $('body');
			var pers = getPersonal(type);
			var qual = toNum($body.data("kv"));
			var kv = getPlayerQual();
			var cityAvg = toNum($body.data("avg-salary"));
			var salary = toNum($body.data("salary"));
			var pct = Math.round((salary / cityAvg) * 100);
			var presReq = Math.max(1, getPersonalNeed(type));
			var qualReq = $body.data("qual-req");
			var minPct = 0, maxPct = Infinity;
			if (type === "office") {
				qualReq = f2(calcQualTop1(kv, presReq, type));
			} else if (type === "shop") {
				minPct = 80;
				maxPct = 300;
				presReq = Math.max(1, Math.ceil(getPersonalNeed(type) * 1.2));
				qualReq = f2(calcQualTop1(kv, Math.ceil(presReq / 0.68), type)); // Временно, чтобы выдержать перегруз
			} else if ((type === "it") || (type === "lab")) {
				presReq = Math.max(1, pers);
			} else if ((type === "workshop") || (type === "mine")) {
				// Пока что самый извратный селектор
				qualReq = toNum($("h2 i.fa-users").parent().parent()
					.find("td:contains('Требуется по технологии')").next().text());
			}
			// Несколько костыльный способ поправить несоотвествие прогноза требуемому навыку
			/*if ($('body').data('qual-fix') !== undefined)
				qualReq = toNum(qualReq) + 0.01;*/
			l("salary: " + salary + ", avg salary: " + cityAvg +
				", pct: " + pct + ", min pct: " + minPct + ", max pct: " + maxPct);
			l("pers: " + pers + ", pers req: " + presReq + ", qual: " + qual + ", qual req: " + qualReq);
			if (((presReq > 0) && (presReq !== pers)) || ((qualReq > 0) && (qualReq !== qual) &&
				(((qualReq > qual) && (pct < maxPct)) || ((qualReq < qual) && (pct > minPct))))) {
				s.setItem("auto-employees-engage-do", "set-pers-qual");
				s.setItem("auto-employees-engage-pers", presReq);
				s.setItem("auto-employees-engage-qual", qualReq);
				s.setItem("auto-employees-min-pct", minPct);
				s.setItem("auto-employees-max-pct", maxPct);
				$('a[href*="employees/engage"]:first').click();
				return true;
			}
			return false;
		}

		function determinant(m) {
			return m[0][0] * m[1][1] * m[2][2] + m[0][2] * m[1][0] * m[2][1] + m[0][1] * m[1][2] * m[2][0] -
				   m[0][2] * m[1][1] * m[2][0] - m[2][2] * m[1][0] * m[0][1] - m[2][1] * m[1][2] * m[0][0];
		}

		function getBestLinearPrice(prices, profit) {
			var pt = profit.slice().sort(function(a,b) { return b - a });
			var bestPrice = prices[profit.indexOf(pt[0])];
			var nextToBestPrice = prices[profit.indexOf(pt[1])];
			// l(bestPrice); l(nextToBestPrice);
			if (bestPrice >= nextToBestPrice)
				return bestPrice * 1.5;
			return bestPrice / 1.5;
		}

		function getBestPrice(prices, profit, prevPrice) {
			l(prices); l(profit);
			if (prices.length === 0) {
				return 0; // Начальная цена уже, наверное, установлена, надо подождать результатов
			} else if (prices.length === 1) {
				if (profit[0] > 0)
					return prices[0] * 1.5; // В качестве теста увеличим цену
				else
					return prices[0] / 4; // Видимо, слишком большая цена
			} else if (prices.length === 2) {
				return getBestLinearPrice(prices, profit);
			}
			// Усилим вес точек так, чтобы каждая последующая после первой весила вдвое меньше
			var mPrices = [], mProfit = [];
			for (var x = 0; x < prices.length; x++) {
				for (var y = Math.pow(prices.length - x, 2); y > 0; y--) {
					mPrices.push(prices[x]);
					mProfit.push(profit[x]);
				}
			}
			var sumX = mPrices.reduce((a, b) => a + b, 0);
			var sumY = mProfit.reduce((a, b) => a + b, 0);
			var sumX2 = mPrices.reduce((a, b) => a + b * b, 0);
			var sumX3 = mPrices.reduce((a, b) => a + b * b * b, 0);
			var sumX4 = mPrices.reduce((a, b) => a + b * b * b * b, 0);
			var sumXY = mPrices.reduce(function(r, a, i) { return r + a * mProfit[i] }, 0);
			var sumX2Y = mPrices.reduce(function(r, a, i) { return r + a * a * mProfit[i] }, 0);
			var n = mPrices.length;
			var delta = determinant([[sumX2, sumX, n], [sumX3, sumX2, sumX], [sumX4, sumX3, sumX2]]);
			var deltaA = determinant([[sumY, sumX, n], [sumXY, sumX2, sumX], [sumX2Y, sumX3, sumX2]]);
			var deltaB = determinant([[sumX2, sumY, n], [sumX3, sumXY, sumX], [sumX4, sumX2Y, sumX2]]);
			var deltaC = determinant([[sumX2, sumX, sumY], [sumX3, sumX2, sumXY], [sumX4, sumX3, sumX2Y]]);
			// l(delta, deltaA, deltaB, deltaC);
			var a = deltaA / delta, b = deltaB / delta, c = deltaC / delta;
			// l(a, b, c);
			var newPrice = -b / a / 2;
			if (a >= 0) {
				// Рога вверх, в центре будет минимум, нам этого не надо
				newPrice = getBestLinearPrice(prices, profit)
			}
			// l(prevPrice); l(newPrice);
			if ((newPrice <= 0) || (prevPrice.toFixed(2) === newPrice.toFixed(2))) {
				var prevProfit = profit[prices.indexOf(prevPrice)];
				var pts = profit.slice().sort(function(a,b) { return b - a });
				// l(prevProfit); l(pts[0]); undefined();
				// Если есть какая-то цена при которой мы когда-то заработали больше
				if ((prevProfit < pts[0]) && (newPrice > 0)) {
					return (newPrice + prices[profit.indexOf(pts[0])]) / 2; // Вернём средее
				} else
					return (prices[profit.indexOf(pts[0])] * (0.95 + Math.random() * 0.1)); // Просто поищем рядом
			}
			return newPrice;
		}

		function setPrice() {
			var prevPrice = toNum($('a[href$=service_history]').text().match(/[^(]+/)[0]);
			var curPrice = toNum($("input[name=servicePrice]").val());
			if (prevPrice !== curPrice)
				return false; // Цену уже поменяли, больше менять не надо
			if (curPrice === 0) {
				// Нет никакой цены, забыл поставить
				$("input[name=servicePrice]").val(10000); // Универсальная цена по умолчанию
				$("input[name=setprice]").click();
				return true;
			}

			s.setItem("auto-service-history-do", "get-data");
			s.setItem("auto-service-history-done", 0);
			$('a[href$=service_history]').click();
			waitAndDo(50, "auto-service-history-done", function () {
				var prices = JSON.parse(s.getItem("auto-service-history-prices"));
				var profit = JSON.parse(s.getItem("auto-service-history-profit"));
				var price = getBestPrice(prices, profit, prevPrice);
				if ((price > 0) && (prices.length > 0)) {
					if (toNum($("body").data("visitors-max")) > 0) {
						var share = toNum($("body").data("visitors")) / toNum($("body").data("visitors-max"));
						l(price); l(price / prevPrice); l(share);
						if (price / prevPrice < share)
							price = prevPrice * share / 0.9; // Занять не более 90% рынка
						else if (share / (price / prevPrice) < 0.05)
							price = prevPrice * share / 0.05; // И не менее 5%
					}
					if (price.toFixed(2) === prevPrice.toFixed(2))
						price *= 0.95 + Math.random() * 0.1;
					$("input[name=servicePrice]").val(price.toFixed(2));
				} else
					$("input[name=servicePrice]").val($("input[name=servicePrice]").val() * (0.95 + Math.random() * 0.1));
				$("input[name=setprice]").click();
			});
			return true;
		}

		function nextUnit() {
			var hrefs = JSON.parse(s.getItem("auto-unit-view-hrefs"));
			if ((hrefs !== null) && (hrefs.length > 0)) {
				var link = hrefs.shift();
				s.setItem("auto-unit-view-hrefs", JSON.stringify(hrefs));
				s.setItem("auto-unit-view-do", "auto");
				window.location.replace(link);
				return;
			}
			s.setItem("auto-unit-view-do", "");
			s.setItem("auto-unit-view-done", 1);
			window.close();
		}

		type = getType(); l(type);
		if (['service', 'medicine', 'repair', 'educational', 'restaurant', 'it'].indexOf(type) !== -1) {
			$("#mainContent").prepend(`<a id="set-price" href="#" style="margin: 10px; display: inline-block; color: blue">set price</a>`);
		}
		$("#mainContent")
			.prepend(`<a id="personnel" href="#" style="margin: 10px; display: inline-block; color: blue">personnel</a>`)
			.prepend(`<a id="save-data" href="#" style="margin: 10px; display: inline-block; color: blue">save data</a>`);
		$("#personnel").click(function (e) {
			e.preventDefault();
			personnel(type);
		});
		$("#set-price").click(function (e) {
			e.preventDefault();
			setPrice();
		});
		$('#save-data').click(function (e) {
			e.preventDefault();
			s.setItem("auto-unit-number", window.location.href.match(/view\/(\d+)/)[1]);
			if (type === 'workshop') {
				var tech = 0;
				var $box = $("i.fa-industry").parent().parent();
				$box.find("tr").each(function () {
					if ($(this).find("td:eq(0)").text() === "Уровень технологии")
						tech = toNum($(this).find("td:eq(1)").text());
				});
				s.setItem("auto-unit-tech", tech);
			} else if (['service', 'medicine', 'repair', 'educational', 'restaurant'].indexOf(type) !== -1) {
				var $body = $('body');
				var visitors = $body.data("visitors");
				if (visitors === undefined) {
					alert("No visitors data!");
					return;
				}
				if (visitors < 1) {
					visitors = $body.data("visitors-max");
					if (visitors === undefined) {
						alert("No max visitors data!");
						return;
					}
				}
				s.setItem("auto-unit-visitors", visitors);
			}
			$(this).css("color", "green");
		}).trigger('click');

		switch (s.getItem("auto-unit-view-do")) {
			case "auto":
				if (personnel(type)) {
					// Страница будет перезагружена, возможно кнопку надо нажать пару раз
					return;
				}
				if (['service', 'medicine', 'repair', 'educational', 'restaurant', 'it'].indexOf(type) !== -1) {
					// Установим цены на услуги
					if (setPrice())
						return;
				}
				// В офисе и др. пока можно только выставить персонал
				if (['office', 'service', 'it', 'lab', 'mine', 'mill', 'animalfarm'].indexOf(type) !== -1) {
					nextUnit();
					return;
				}
				s.setItem("auto-unit-view-do", "");
				if ((type === "shop") || (type === "fuel")) {
					s.setItem("auto-trading-hall-do", "auto");
					window.location.replace($('a[data-name=itour-tab-unit-view--trading_hall]').prop("href"));
				} else if (['workshop', 'medicine', 'repair', 'educational', 'restaurant'].indexOf(type) !== -1) {
					s.setItem("auto-supply-hall-do", "auto");
					window.location.replace($('a[data-name=itour-tab-unit-view--supply]').prop("href"));
				}
				return;
			case "check":
				// todo: checks
				nextUnit();
				return;
			default:
				return;
		}
	}

	// <<< Employees engage
	if (/window\/unit\/employees\/engage\/\d+$/.test(window.location.href)) {
		function countSalary(avg_kv, avg_zp, find_kv, minPct, maxPct) {
			//если зп < 100% то требуемая_зп = КОРЕНЬ(треб_квала/базовая_квала)*городская_зп
			// y = 2* (0.5)^base
			var current_kv = $("#apprisedEmployeeLevel").text();
			var my_salary = toNum($("#salary").val());

			var k = my_salary / avg_zp;
			if (k < 1) {
				var base = (current_kv / k / k);
				var val = Math.sqrt(find_kv / base) * avg_zp;
				// если зарплата превысила среднею
				var limit = val / avg_zp;
				if (limit > 1) {
					base = 2 * Math.pow(0.5, base / avg_kv);
					val = (Math.pow(2, find_kv / avg_kv) * base - 1) * avg_zp;
				}
			} else {
				base = (k + 1) / Math.pow(2, current_kv / avg_kv);
				val = (Math.pow(2, find_kv / avg_kv) * base - 1) * avg_zp;
				// если зарплата стала меньше средней
				limit = val / avg_zp;
				if (limit < 1) {
					base = avg_kv * Math.log(base / 2) / Math.log(0.5);
					val = Math.sqrt(find_kv / base) * avg_zp;
				}
			}
			var pct = 100 * val / avg_zp;
			if ((minPct !== undefined) && (pct < minPct)) {
				val = avg_zp * minPct / 100;
			}
			if ((maxPct !== undefined) && (pct > maxPct)) {
				val = avg_zp * maxPct / 100;
			}
			return val.toFixed(2);
		}

		var $salary = $('#salary');
		var avg_zp = /([\D]+)([\d\s]+\.*\d*)/.exec($("table.list td:contains(Средняя зарплата)").text())[2].replace(" ", "");
		/(\d+\.*\d+)\D*(\d+\.*\d+)/.exec($("span:contains(требуется)").text());
		var avg_kv = toNum(RegExp.$1);
		var find_kv = toNum(RegExp.$2);

		var container = $('div.headerSeparator');
		var input_95 = $('<button id=b_kv title="Выставить зарплату по квалификации">1:1</button>').click(function () {
			$salary.prop('value', countSalary(avg_kv, avg_zp, find_kv));
			appriseEmployeeLevel();
		});
		var input_100 = $('<button id=b100>100%</button>').click(function () {
			$salary.prop('value', avg_zp);
			appriseEmployeeLevel();
		});
		var input_f = $("<input> ").prop({type: 'text', value: '', size: '6'}).change(function () {
			$salary.prop('value', countSalary(avg_kv, avg_zp, this.value));
			appriseEmployeeLevel();
		});
		container.append($('<table><tr>').append('<td>').append(input_95).append('<td>')
			.append(input_f).append('<td>').append(input_100).append('<td>'));

		var $form = $("form").attr("onsubmit", "");
		switch (s.getItem("auto-employees-engage-do")) {
			case "set-pers-qual":
				$("#quantity").val(s.getItem("auto-employees-engage-pers"));
				$salary.prop('value', countSalary(avg_kv, avg_zp, s.getItem("auto-employees-engage-qual"),
					s.getItem("auto-employees-min-pct"), s.getItem("auto-employees-max-pct")));
				$form.submit();
				break;
		}
		s.setItem("auto-employees-engage-do", "");
	}

	// Service history
	if (/window\/unit\/view\/\d+\/service_history$/.test(window.location.href)) {
		if (s.getItem("auto-service-history-do") === "get-data") {
			var prices = [], profit = [];
			$("table.list tbody tr:gt(0)").each(function () {
				if (prices.length === 10)
					return; // Последних 10 значений достаточно
				var price = toNum($(this).find("td:eq(1)").text());
				if (prices.indexOf(price) !== -1)
					return; // Эта цена уже добавлена с более актуальным профитом
				prices.push(price);
				profit.push(toNum($(this).find("td:eq(3)").text()));
			});
			s.setItem("auto-service-history-prices", JSON.stringify(prices));
			s.setItem("auto-service-history-profit", JSON.stringify(profit));
			s.setItem("auto-service-history-do", "");
			s.setItem("auto-service-history-done", 1);
			window.close();
		}
	}

	// <<< Trading hall
	if (/main\/unit\/view\/\d+\/trading_hall$/.test(window.location.href)) {
		function parseStock(todo) {
			var data = {};
			$("form[name=tradingHallForm] > table:eq(0) > tbody").children("tr").each(function () {
				let $tr = $(this);
				if ($tr.children("td").length > 1) {
					let $td10 = $tr.children("td:eq(9)");
					let $priceInput = $td10.children("input:eq(0)");

					const itemType = $tr.children("td:eq(2)").attr("title").replace(/^(.*) \(.*$/, "$1").trim();
					let itemId = $tr.children("td:eq(2)").find("a").prop("href").match(/by_trade_at_cities\/(\d+)\//)[1];
					let itemSold = toNum($tr.children("td:eq(3)").find("a").text());
					let itemOrdered = toNum($tr.children("td:eq(4)").text().replace(/\s/g, "").match(/\[(\d+)]/)[1]);
					let itemOrderedToday = toNum($tr.find("td:eq(4) a").text());
					let itemCount = toNum($tr.children("td:eq(5)").text());
					let itemQuality = toNum($tr.children("td:eq(6)").text());
					let itemBrand = toNum($tr.children("td:eq(7)").text());
					let itemCost = toNum($tr.children("td:eq(8)").text());

					const cityPrice = toNum($tr.children("td:eq(11)").text());
					const cityQuality = toNum($tr.children("td:eq(12)").text());
					const cityBrand = toNum($tr.children("td:eq(13)").text());
					const marketShare = toNum($tr.children("td:eq(10)").text());

					// let currentPrice = toNum($priceInput.val());

					data[itemId] = {
						type: itemType,
						sold: itemSold,
						ordered: itemOrdered,
						count: itemCount,
						price: cityPrice,
						quality: cityQuality,
						share: marketShare,
					};

					let targetPrice;
					let minPrice = 0;
					let maxPrice = 1000000000;

					if (itemCost.toString() !== "NaN") {
						minPrice = itemCost * 2;
						// Если продаж и закупок было < 1/20 склада, снижать норму прибыли до 10%
						if ((itemCount / 20 > itemSold) && (itemCount / 20 > itemOrdered))
							minPrice = itemCost * 1.1;
					} else {
						// itemCost = cityPrice;
						itemQuality = cityQuality;
						itemBrand = cityBrand;
					}

					targetPrice = cityPrice;
					targetPrice *= Math.log(itemQuality / cityQuality + 1.71828182846);
					targetPrice *= Math.log((1 + itemBrand) / (1 + cityBrand) + 1.71828182846);
					targetPrice *= getShareMul(marketShare);
					targetPrice = Math.max(targetPrice, minPrice);
					targetPrice = Math.min(targetPrice, maxPrice);

					if (todo === "setPrices")
						$priceInput.val(targetPrice.toFixed(2));
					if ((todo === "clearUnused") && (itemSold === 0) && (itemOrdered === 0) &&
						(itemCount === 0) && (itemOrderedToday === 0))
						$tr.find("input[type=checkbox]").prop("checked", true);
				}
			});
			if (todo === "storeData") {
				s.setItem("auto-store-prices", JSON.stringify(data));
				var $title = $("td.title:eq(0)");
				if ($title.length > 0)
					s.setItem("auto-product-category", $title.html().match(/city_market\/(\d+)/)[1]);
				else
					s.setItem("auto-product-category", -1);
				s.setItem("auto-unit-number", window.location.href.match(/view\/(\d+)\/trading_hall/)[1]);
			}
			if (todo === "clearUnused")
				setActionAndSubmit("terminate");
		}

		$("select[name=productCategory]")
			.before(`<a id="storeData" href="#" style="margin-right: 20px">store data</a>`)
			.before(`<a id="setPrices" href="#" style="margin-right: 20px">set prices</a>`)
			.before(`<a id="updatePrices" href="#" style="margin-right: 20px">update prices</a>`)
			.before(`<a id="clearUnused" href="#" style="margin-right: 20px">clear unused</a>`);
		$('#setPrices, #storeData, #clearUnused').click(function (e) {
			e.preventDefault();
			parseStock($(this).prop("id"));
			$(this).css("color", "green");
		});
		$('#storeData').trigger('click');
		$('#updatePrices').click(function (e) {
			e.preventDefault();
			$("input[name=setprice]").click();
		});

		switch (s.getItem("auto-trading-hall-do")) {
			case "auto":
				parseStock('storeData');
				parseStock('setPrices');
				s.setItem("auto-trading-hall-do", "auto2");
				$("input[name=setprice]").click();
				break;
			case "auto2":
				s.setItem("auto-trading-hall-do", "stock");
				parseStock('clearUnused');
				break;
			case "stock":
				s.setItem("auto-trading-hall-do", "");
				s.setItem("auto-supply-hall-do", "auto");
				window.location.replace($('a[data-name=itour-tab-unit-view--supply]').prop('href'));
				break;
			default:
		}
	}

	// <<< Supply hall
	if (/main\/unit\/view\/\d+\/supply$/.test(window.location.href)) {
		function checkStoreId() {
			if (s.getItem("auto-unit-number") !== window.location.href.match(/view\/(\d+)\/supply/)[1]) {
				alert("Wrong unit!");
				return false;
			}
			return true;
		}

		function balance(multiplier) {
			// Для каждого ряда селектор сработает дважды, и это жесть
			var $available = $('td:contains(Свободно)').parent();
			var $apply = $("input[name=applyChanges]");
			if ($available.length > 0) {
				var post = false, remove = false;
				$available.each(function () {
					let $td = $(this).find("td:eq(1)");
					let free = toNum($td.text());
					if ($td.text().trim() === "Не огр.")
						free = 10000000;
					if (free > 0) {
						let searchId = $td.prop("id").replace("free_", "quantityField_");
						if (searchId < 1) // Заводы и т.п.
							searchId = $td.parent().parent().parent().parent().prop("id").replace("quantity_", "quantityField_");
						var $input = $("#" + searchId + " input");
						$input.addClass("mySet");
						if ($input.attr("data-set") === undefined)
							$input.attr("data-set", free);
						else
							$input.attr("data-set", Math.min($input.attr("data-set"), free));
					} else {
						var $checkbox = $(this).parent().parent().parent().next().find("input");
						if ($checkbox.length > 0) {
							$checkbox.prop('checked', true);
							remove = true;
						}
					}
				});
				// Разорвём контракты со своими юнитами
				$('td strong').each(function () {
					if (['shop', 'fuel'].indexOf(getType()) !== -1)
						$('td strong').parent().parent().parent().find("input.destroy").prop('checked', true);
					else
						$('td strong').parent().parent().find("input.destroy").prop('checked', true);
					remove = true;
				});
				$("input.mySet").each(function () {
					var set = $(this).attr("data-set");
					var max = toNum($(this).parent().find('span').text());
					if (max !== 0)
						set = Math.min(set, max);
					set = Math.min(set, 10000000);
					if (toNum($(this).val()) !== toNum(set)) {
						l($(this).val() + " " + set);
						$(this).val(set);
						post = true;
					}
				});
				if (post) {
					$apply.click();
					return;
				}
				if (remove) {
					$("input[name=destroy]").prop("onclick", `return true`).click();
					return;
				}
			}

			s.setItem("auto-supply-hall-balance", 0);
			if ($apply.length > 0)
				$apply.click();
			else
				window.location = window.location.href; // Надо перезагрузить страницу, чтобы всё нормально сработало
		}

		// Похожа на balance, но удаляет ненужные ордера (где заказано 1 и pq хреновый) после перезаказа
		function cancel1() {
			var delta = ckeckReorder();
			if (delta !== 0) {
				// Злобный ордер, который не видно при реордере
				var $bad = $('input[name^="supplyContractData[party_quantity]"][value=' + delta + ']');
				if ($bad.length > 0) {
					$bad.attr("value", 1); // Сейчас уберём его
				} else {
					// alert("Reorder ajax failed!");
					s.setItem("auto-supply-hall-do", "auto");
					window.location = window.location.href; // Остановим всё, чтобы можно было перезагрузить страницу
				}
			}

			var $empty = $("input[value=1][name^=supplyContractData]");
			if ($empty.length > 0) {
				$empty.each(function () {
					var $row = $(this).parent().parent();
					var id = $row.attr("id").match(/\d+/)[0];
					// Найдём все инпуты этого товара
					// Если среди них есть не пустой, где есть заказ
					if ($("tr[id*=" + id + "] td[id^=quantityField] input[value!=1][name^=supplyContractData]").length > 0) {
						// Отметим этот ордер на удаление
						$row.find("input[type=checkbox]").prop("checked", true);
					}
				})
			}
			if ($("input[name=destroy]").length > 0)
				$("input[name=destroy]").prop("onclick", `return true`).click();
			else
				window.location = window.location.href; // Надо перезагрузить страницу, чтобы всё нормально сработало
		}

		function ckeckReorder() {
			var total = toNum(s.getItem("auto-reorder-total"));
			var sum = 0;
			$('input[name^="supplyContractData[party_quantity]"]').each(function () {
				sum += toNum($(this).val());
			});
			l("t: " + total + ", s: " + sum);
			// Есть очень злой баг, когда ордер просто не отображается, если разница не сильно велика, хрен с ним
			// return Math.abs(total - sum) / (total + 1) < 0.05;
			return Math.abs(total - sum)
		}

		function countSubtotal() {
			var subtotal = toNum(s.getItem("auto-reorder-subtotal"));
			s.setItem("auto-reorder-total", toNum(s.getItem("auto-reorder-total")) + subtotal);
			l("s: " + subtotal);
		}

		function reorder() {
			if (!checkStoreId())
				return;

			s.setItem("auto-reorder-total", 0);
			if ((getType() === "shop") || (getType() === "fuel")) {
				let category = s.getItem("auto-product-category");
				if (category > 0)
					$("select[name=productCategory]").val(category);

				drowProducts(); // Это не я так функцию назвал
				s.setItem("auto-supply-create-type", "shop");
				var $prods = $("#productsHereDiv").find("a");
				var doNext = function (iter) {
					var id = $($prods[iter]).attr("href").match(/\d+/)[0];
					s.setItem("auto-supply-create-product-id", id);
					s.setItem("auto-supply-create-do", "reorder");
					s.setItem("auto-supply-create-done", 0);
					selectSupplyProduct(id);
					if (iter + 1 < $prods.length)
						waitAndDo(50, "auto-supply-create-done", function () {
							countSubtotal();
							doNext(iter + 1);
						});
					else
						waitAndDo(50, "auto-supply-create-done", function () {
							countSubtotal();
							s.setItem("auto-supply-hall-reorder-done", 1);
						});
				};
				doNext(0);
			} else if (getType() === "workshop") {
				s.setItem("auto-supply-create-type", "workshop");
				var $buttons = $(`img[title="Выбрать поставщика"]`).parent();
				doNext = function (iter) {
					var id = $($buttons[iter]).attr("href").match(/\d+$/)[0];
					s.setItem("auto-supply-create-product-id", id);
					s.setItem("auto-supply-create-do", "reorder");
					s.setItem("auto-supply-create-done", 0);
					$buttons[iter].click();
					if (iter + 1 < $buttons.length)
						waitAndDo(50, "auto-supply-create-done", function () {
							countSubtotal();
							doNext(iter + 1);
						});
					else
						waitAndDo(50, "auto-supply-create-done", function () {
							countSubtotal();
							s.setItem("auto-supply-hall-reorder-done", 1);
						});
				};
				doNext(0);
			}
		}

		function apprise() {
			if (!checkStoreId())
				return;

			s.setItem("auto-reorder-total", 0);
			s.setItem("auto-supply-create-type", getType());
			var $buttons = $(`img[title="Выбрать поставщика"]`).parent();
			var positions = [], steps = [];
			var doNext = function (iter) {
				s.setItem("auto-supply-create-do", "apprise");
				s.setItem("auto-supply-create-done", 0);
				$buttons[iter].click();
				if (iter + 1 < $buttons.length)
					waitAndDo(50, "auto-supply-create-done", function () {
						positions.push(toNum(s.getItem("auto-unit-apprise-opt-pos")));
						steps.push(JSON.parse(s.getItem("auto-unit-apprise-steps")));
						doNext(iter + 1);
					});
				else {
					waitAndDo(50, "auto-supply-create-done", function () {
						positions.push(toNum(s.getItem("auto-unit-apprise-opt-pos")));
						steps.push(JSON.parse(s.getItem("auto-unit-apprise-steps")));
						s.setItem("auto-supply-hall-apprise-done", 0);

						var pqTotal = 0, noQualUp = false, noQualDown = false;
						// l(steps); l(positions); undefined();
						/*if (positions.length > 1)*/ {
							while (1) {
								var minStep, maxStep, minQual = Infinity, maxQual = 0, total = 0;
								$.each(steps, function (k, v) {
									if (v[positions[k]].quality > maxQual) {
										maxQual = v[positions[k]].quality;
										maxStep = k;
									}
									if (v[positions[k]].quality < minQual) {
										minQual = v[positions[k]].quality;
										minStep = k;
									}
									total += v[positions[k]].cost;
								});
								if (pqTotal === 0) {
									// Изначально все позиции стоят на оптимуме по цене/качеству
									pqTotal = total * 2; // Но можно тратить больше
									l(pqTotal);
								}
								if (!noQualUp && (total < pqTotal)) {
									// Можно попробовать поднять качество
									if ((steps[minStep].length - 1 > positions[minStep]) /*&&
										(steps[minStep][positions[minStep] + 1].quality <= maxQual)*/) {
										positions[minStep]++;
									} else
										noQualUp = true; // Поднимать качество некуда или мы поднимаем качество выше всех остальных товаров
								} else if (!noQualDown) {
									// Снижаем цену
									if ((positions[maxStep] > 0) &&
										(steps[maxStep][positions[maxStep] - 1].quality >= minQual)) {
										positions[maxStep]--;
									} else
										noQualDown = true; // Опускать цену некуда или мы опускаем качество ниже всех остальных товаров
								}
								if ((noQualUp || (total >= pqTotal)) && noQualDown)
									break;
							}
						}
						var doOrder = function (iter) {
							s.setItem("auto-supply-create-do", "reorder");
							s.setItem("auto-supply-create-done", 0);
							s.setItem("auto-supply-create-cost", steps[iter][positions[iter]].cost);
							s.setItem("auto-supply-create-quality", steps[iter][positions[iter]].quality);
							l(steps[iter][positions[iter]].cost + " " + steps[iter][positions[iter]].quality);
							$buttons[iter].click();
							if (iter + 1 < $buttons.length)
								waitAndDo(50, "auto-supply-create-done", function () {
									countSubtotal();
									doOrder(iter + 1);
								});
							else
								waitAndDo(50, "auto-supply-create-done", function () {
									countSubtotal();
									s.setItem("auto-supply-hall-apprise-done", 1);
								});
						};
						doOrder(0);
					});
				}
			};
			doNext(0);
		}

		if (s.getItem("auto-supply-hall-balance") > 0) {
			balance(1);
			return;
		}

		type = getType();
		$("select[name=productCategory]") // Магазин
			.before(`<a id="balance" href="#" style="margin-right: 20px">balance</a>`)
			.before(`<a id="reorder" href="#" style="margin-right: 20px">reorder all</a>`)
			.before(`<a id="reorderSelected" href="#" style="margin-right: 20px">reorder selected</a>`)
			// .before(`<a id="orderHalf" href="#" style="margin-right: 20px">order half</a>`)
			.before(`<a id="cancel1" href="#" style="margin-right: 20px">cancel 1</a>`);
		if (type === "workshop") {
			qualNeed = Math.pow(s.getItem("auto-unit-tech"), 1.3);
			$("table.list:eq(0)")
				.before(`<a id="balance" href="#" style="margin-right: 20px">balance</a>`)
				.before(`<a id="reorder" href="#" style="margin-right: 20px">reorder all (` + qualNeed.toFixed(2) + `)</a>`)
				.before(`<a id="cancel1" href="#" style="margin-right: 20px">cancel 1</a>`)
		} else if (['medicine', 'repair', 'educational', 'restaurant'].indexOf(type) !== -1) {
			$("table.list:eq(0)")
				.before(`<a id="balance" href="#" style="margin-right: 20px">balance</a>`)
				.before(`<a id="apprise" href="#" style="margin-right: 20px">apprise</a>`)
				.before(`<a id="cancel1" href="#" style="margin-right: 20px">cancel 1</a>`)
		}

		$('#balance').click(function (e) {
			e.preventDefault();
			if (!checkStoreId())
				return;
			s.setItem("auto-supply-hall-balance", 1);
			balance(1);
		});
		$('#apprise').click(function (e) {
			e.preventDefault();
			apprise();
		});
		$('#reorder').click(function (e) {
			e.preventDefault();
			reorder();
		});
		$('#cancel1').click(function (e) {
			e.preventDefault();
			cancel1();
		});
		$('#reorderSelected').click(function (e) {
			e.preventDefault();
			if (!checkStoreId())
				return;
			var $prod = $("input[name=product]:checked");
			s.setItem("auto-supply-create-do", "reorder");
			s.setItem("auto-supply-create-done", 0);
			s.setItem("auto-supply-create-product-id", $prod.val());
			s.setItem("auto-supply-create-dont-close", 1);
		});
		$('#orderHalf').click(function (e) {
			e.preventDefault();
			if (!checkStoreId())
				return;
			s.setItem("auto-supply-hall-balance", 1);
			balance(0.5);
		});

		// Сначала надо убедиться, что балансировка закончена
		if (s.getItem("auto-supply-hall-balance") < 1) {
			switch (s.getItem("auto-supply-hall-do")) {
				case "auto":
					if (['medicine', 'repair', 'educational', 'restaurant'].indexOf(type) !== -1)
						s.setItem("auto-supply-hall-do", "apprise");
					else
						s.setItem("auto-supply-hall-do", "reorder");
					s.setItem("auto-supply-hall-balance", 1);
					balance(1); // It will reload
					break;
				case "reorder":
					s.setItem("auto-supply-hall-reorder-done", 0);
					s.setItem("auto-supply-hall-do", "");
					reorder();
					waitAndDo(50, "auto-supply-hall-reorder-done", function () { // Дождались конца реордера
						s.setItem("auto-supply-hall-do", "cancel1");
						window.location = window.location.href;
					});
					break;
				case "apprise":
					s.setItem("auto-supply-hall-apprise-done", 0);
					s.setItem("auto-supply-hall-do", "");
					apprise();
					waitAndDo(50, "auto-supply-hall-apprise-done", function () { // Дождались конца реордера
						s.setItem("auto-supply-hall-do", "cancel1");
						window.location = window.location.href;
					});
					break;
				case "cancel1":
					s.setItem("auto-supply-hall-do", "out");
					cancel1(); // It will reload
					break;
				case "out":
					s.setItem("auto-supply-hall-do", "");
					s.setItem("auto-unit-view-do", "check");
					window.location.replace($('a[data-name=itour-tab-unit-view]').prop("href"));
					break;
				default:
			}
		}
	}

	// <<< Supply create
	if (/window\/unit\/supply\/create\/\d+\/step2$/.test(window.location.href)) {
		// <<< pq
		function FillArray(id, cen) {
			this.id = id;
			this.cen = cen;
		}

		var txt = [];
		var i = 0;
		var $content = $('#supply_content');
		$content.find('table tr').each(function () {
			var temp = $(this).attr('id');
			if (!isNaN(temp)) {
				return;
			}

			var cels1 = $('th', this);
			$(cels1[3]).after('<th><div class="field_title">ЦК<div class="asc" title="сортировка по возрастанию"><a id="qpasc" href="#"><img src="/img/up_gr_sort.png"></a></div><div class="desc" title="сортировка по убыванию"><a id="qpdesc" href="#"><img src="/img/down_gr_sort.png"></a></div></div></th>');

			var cels = $('td', this);
			var price = parseFloat($(cels[5]).text().replace(/[^\d.]+/g, ''));
			var qual = parseFloat($(cels[6]).text().replace(/[^\d.]+/g, ''));

			if (isNaN(price) || isNaN(qual)) {
				return;
			}

			var qp = (price / qual).toFixed(2);
			i++;
			$(cels[5]).after('<td class="supply_data" id="td_s' + i + '" style="color: #f00">' + qp + '</td>');
			txt[i] = new FillArray(i, parseFloat($('#td_s' + i).text()));
		});

		var total = i;

		function sort_table(type) {
			txt.sort(function (a, b) {
				if (type === 'asc') {
					return (a.cen > b.cen) ? 1 : ((b.cen > a.cen) ? -1 : 0);
				} else {
					return (a.cen < b.cen) ? 1 : ((b.cen < a.cen) ? -1 : 0);
				}
			});
			for (i = total - 1; i > 0; i--) {
				var id_rod = $('#td_s' + txt[i]['id']).closest('tr');
				var id_rod1 = $('#td_s' + txt[i - 1]['id']).closest('tr');

				if (id_rod1.next().hasClass('ordered')) {
					var n = id_rod1.next();
					id_rod.before(id_rod1).before(n);
				} else
					id_rod.before(id_rod1);

			}
			return false;
		}

		$('#qpasc').click(function () {
			sort_table('asc');
			return false;
		});

		$('#qpdesc').click(function () {
			sort_table('desc');
			return false;
		});
		// >>>

		let todo = s.getItem("auto-supply-create-do");
		if (todo.length < 1)
			return;

		// Все поставщики на одной странице
		let $li = $("ul.pager_options.pull-left li.selected");
		if ($li.length > 0) {
			window.location.assign($li.next().find("a").prop("href") + "000"); // 100000 элементов
			return;
		}

		// Отобразить всех поставщиков
		if ($("#FilterClear").length === 0) {
			$("#FilterFadeIn").click();
			$("input[name='free_for_buy[from]']").val("");
			$("input[name='quality[from]']").val("1");
			setFilter();
			return;
		}

		function getRowData($row) {
			var available;
			if ($row.find("td:eq(3)").text().trim() === "Не огр.")
				available = 10000000; // Infinity плохо работает
			else if ($row.find("td:eq(3) u").length > 0) // Красная
				available = toNum($row.find("td:eq(3) u").text());
			else
				available = parseFloat($row.find("td:eq(3)").html().trim().replace(/\s/, ""));
			var price = toNum($row.find("td:eq(5)").text());
			var quality = toNum($row.find("td:eq(7)").text());
			var brand = toNum($row.find("td:eq(8)").text());
			var ordered = 0;
			if ($row.hasClass("ordered_offer")) {
				ordered = toNum($row.next().find("span.orderedAmount").text());
				if ($row.find("td:eq(3) u").length < 1) // Не красная надпись!
					available += ordered;
			}
			if ($row.find('td.brandname_img img[src^="/img/products/brand/"]').length > 0) {
				available = 0; // Ингорируем франшизы
			}
			return [available, ordered, price, quality, brand];
		}

		function getListData(list) {
			var pqOrder = 0, pqPrice = 0, pqQuality = 0;
			$.each(list, function (k, v) {
				pqPrice = (pqPrice * pqOrder + v.price * v.order) / (pqOrder + v.order);
				pqQuality = (pqQuality * pqOrder + v.quality * v.order) / (pqOrder + v.order);
				pqOrder += v.order;
			});
			return [pqPrice, pqQuality, pqOrder];
		}

		function getListMinQuality(list) {
			var p = 0, q = Infinity, id = 0;
			$.each(list, function (k, v) {
				// Выберем менее качественное или более дорогое
				if ((v.quality < q) || ((v.quality === q) && (v.price > p))) {
					p = v.price;
					q = v.quality;
					id = k;
				}
			});
			return id;
		}

		function getListMaxPrice(list) {
			var p = 0, q = Infinity, id = 0;
			$.each(list, function (k, v) {
				// Выберем более дорогое или с меньшим качеством
				if ((v.price > p) || ((v.price === p) && (v.quality < q))) {
					p = v.price;
					q = v.quality;
					id = k;
				}
			});
			return id;
		}

		function getListFromRows($rows) {
			list = [];
			$rows.each(function () {
				var available, ordered, price, quality, brand;
				[available, ordered, price, quality, brand] = getRowData($(this));
				if (available > 0)
					list.push({
						id: $(this).attr("id"),
						price: price,
						quality: quality + brand,
						available: available,
						ordered: ordered,
						pq: price / (quality + brand), // todo: м.б. всё-таки выделить бренд отдельно?
					});
			});
			list.sort(function (a, b) { // ASC
				return a.pq - b.pq;
			});
			return list;
		}

		function getPQ(pqList, required) {
			var pqOrder = 0, pqPrice = 0, pqQuality = 0;
			$.each(pqList, function (k, v) {
				if (pqOrder >= required)
					return; // Всё что нужно посчитали
				var tmpOrder = Math.min(required - pqOrder, v.available);
				// Складываем средневзвешенно
				pqPrice = (pqPrice * pqOrder + v.price * tmpOrder) / (pqOrder + tmpOrder);
				pqQuality = (pqQuality * pqOrder + v.quality * tmpOrder) / (pqOrder + tmpOrder);
				pqOrder += tmpOrder;
			});
			return [pqPrice, pqQuality];
		}

		function makeList(type, pqList, required, qualNeed, maxPrice, cityPrice) {
			while (1) {
				var list = {}, pqPrice, pqQuality, pqOrder;
				var req01 = Math.max(1, Math.floor(required / 10)), qn005 = qualNeed / 20;
				$.each(pqList, function (k, v) {
					[pqPrice, pqQuality, pqOrder] = getListData(list);
					var diff = (pqQuality - qualNeed) / qualNeed;
					if ((pqOrder >= required) && (diff > -0.01) && (diff < 0.1))
						return;
					if ((pqOrder >= required) && ((v.price > pqPrice) && (v.quality < pqQuality)))
						return; // Этот поставщик не улучшит ситуацию
					/*if ((type === "shop") && (v.price > cityPrice))
						return; // Товары дороже, чем в городе нам не нужны*/
					// Добавляем весь товар поставщика в список, после чего балансируем
					list[$(this).attr("id")] = {
						price: v.price,
						quality: v.quality,
						order: v.available,
					};
					[pqPrice, pqQuality, pqOrder] = getListData(list);
					while (pqOrder > required) {
						// l(list); undefined();
						// Убираем кусочками
						var toRemove = Math.min(
							Math.max(10, Math.round((pqOrder - required) / 100)), // Либо 10, либо 1% разницы
							pqOrder - required); // Но убрать так, чтобы осталось не меньше, чем требуется
						if (pqQuality > qualNeed) {
							// Убираем из списка товары с максимальной ценой, пока не останется сколько нужно
							var id = getListMaxPrice(list);
						} else {
							// Убираем из списка товары с минимальным качеством, пока не останется сколько нужно
							id = getListMinQuality(list);
						}
						if (list[id] === undefined) { // Прошли весь список и ничто не подошло?
							pqPrice = maxPrice * 2; // Чтобы исполнилось условие понижения планки
							break;
						}
						/*if (list[id].order === Infinity) // Независимый поставщик
							list[id].order = required;*/
						// Если надо убрать больше, чем в заказе с максимальной ценой
						if (toRemove >= list[id].order) {
							// Удалим заказ целиком
							delete list[id];
						} else {
							list[id].order -= toRemove;
						}
						[pqPrice, pqQuality, pqOrder] = getListData(list);
					}
				});
				var diff = (pqQuality - qualNeed) / qualNeed;
				// Если заказать нужное качество слишком дорого
				if ((pqPrice > maxPrice) || (diff < -0.01)) {
					if (['medicine', 'repair', 'educational', 'restaurant'].indexOf(type) !== -1) {
						break; // Там уже всё подобрано, если не удалось найти, значит слишком большой заказ
					} else if (type === "shop") {
						// Попробуем купить меньше
						required -= req01;
						if (required < 1)
							return {};
					} else {
						// Понизим планку
						qualNeed -= qn005;
						// Если снизить цену через качество нельзя
						if (qualNeed < 1)
							break;
					}
					continue;
				}
				break;
			}
			// Костыль. Такого не должно наступать
			/*if ((type === "shop") && (diff < -0.01))
				return {};*/
			return list;
		}

		var type = s.getItem("auto-supply-create-type"); l(type);
		var $info = $("div.supply_addition_info table tr");
		var ajaxClick = 0, maxPrice = 0;
		switch (todo) {
			case "reorder":
				var cityPrice = 0;
				var id = s.getItem("auto-supply-create-product-id");
				if (type === "shop") {
					cityPrice = toNum($info.find("th:eq(0)").text().replace("Цена = ", ""));
					var cityQuality = toNum($info.find("th:eq(1)").text().replace("Качество = ", ""));
					cityQuality += toNum($info.find("th:eq(2)").text().replace("Бренд = ", "")); // Может быть, следует отделить
					var required = Math.floor(1000000 / cityPrice);
					var qualNeed = cityQuality * 1.2;
					var prices = JSON.parse(s.getItem("auto-store-prices"));
					if (prices[id] !== undefined) {
						if (prices[id].ordered < prices[id].count) { // Если купили меньше, чем на складе сейчас
							required = prices[id].sold * 4; // На складе должно быть в 4 раза больше, чем продано
							required -= prices[id].count; // Минус сколько уже есть
						} else { // Всё скупили
							required = 2 * Math.max(required,
									Math.min(prices[id].sold * 4, prices[id].ordered),
									prices[id].sold);
						}
						// qualNeed = cityQuality / getShareMul(prices[id].share);
						// Не покупать больше, чем 2 размера рынка
						if ((prices[id].sold > 0) && (prices[id].share > 0))
							required = Math.min(required, 2 * prices[id].sold / (prices[id].share / 100));
					}
				} else if (type === "workshop") {
					// Поддерживать 4-кратный объём требуемого
					required = toNum($info.find("th:contains(Требуется)").text().replace("Требуется = ", "")) * 4 -
						toNum($info.find("th:contains(Количество на складе)").text().replace("Количество на складе = ", ""));
					qualNeed = Math.pow(s.getItem("auto-unit-tech"), 1.3);
				} else if (['medicine', 'repair', 'educational', 'restaurant'].indexOf(type) !== -1) {
					visitors = toNum(s.getItem("auto-unit-visitors"));
					perVisit = toNum($info.find("th:contains(Расх. на клиента)").text().replace("Расх. на клиента", ""));
					var weekly = visitors * perVisit;
					required = weekly * 4 - toNum($info.find("th:contains(Количество на складе)").text());
					qualNeed = toNum(s.getItem("auto-supply-create-quality"));
					maxPrice = toNum(s.getItem("auto-supply-create-cost")) / weekly;
				}

				$("div.summary").append(`<span style="margin-right: 20px">
					Надо: <span id='required'>` + required + `</span>,
					После: <span id='left'></span>
				</span>`);

				var pqList = getListFromRows($content.find("table tbody tr.wborder, table tbody tr.ordered_offer"));
				if (required > 0) {
					// Посчитаем качество и цену, если заказывать по минимальному pq
					var pqOrder = 0, pqPrice = 0, pqQuality = 0;
					[pqPrice, pqQuality] = getPQ(pqList, required);
					l("p: " + pqPrice + ", cp:" + cityPrice + ", q:" + pqQuality + ", cq:" + cityQuality);
					// Всё, что мы считали выше, было нужно для оценки максимальной цены
					if (maxPrice === 0) {
						if (type === "shop")
							maxPrice = cityPrice; // Math.min(cityPrice, pqPrice * 1.2);
						else
							maxPrice = pqPrice * 1.2;
					}
					// Теперь попробуем приблизить качество к требуемому либо снижая его и цену, либо наоборот
					var list = makeList(type, pqList, required, qualNeed, maxPrice, cityPrice);
					[pqPrice, pqQuality, pqOrder] = getListData(list);
					l("pqp: " + pqPrice + ", pqq:" + pqQuality); l(list);
				}
				$('#left').html(required - pqOrder);
				// Теперь закажем всё, что выбрали, а что не выбрали выставим в 1
				total = 0;
				$.each(pqList, function (k, v) {
					var required = 0;
					if (list[v.id] !== undefined)
						required = Math.floor(list[v.id].order);
					if ((required < 1) && (v.ordered !== 0)) { // Установить минимальный размер заказа, чтобы не терять поставщика
						required = 1;
					}
					if (required > 0) {
						$("#" + v.id).find("td.choose").click();
						$("#amountInput").val(required);
						$("#submitLink").click();
						ajaxClick++;
						total += required;
					}
				});
				s.setItem("auto-reorder-subtotal", total);
				break;
			case "apprise":
				var visitors = toNum(s.getItem("auto-unit-visitors"));
				var perVisit = toNum($info.find("th:contains(Расх. на клиента)").text().replace("Расх. на клиента", ""));
				// Расход на одного клиента за день
				required = visitors * perVisit;
				pqList = getListFromRows($content.find("table tbody tr.wborder, table tbody tr.ordered_offer"));
				// Посчитаем качество и цену, если заказывать по минимальному pq
				[pqPrice, pqQuality] = getPQ(pqList, required);
				l(pqPrice); l(pqQuality); l(required);
				var optCost = required * pqPrice, optQuality = pqQuality;

				var steps = [], newQualNeed;
				maxPrice = pqPrice; qualNeed = 1;
				do {
					list = makeList(type, pqList, required, qualNeed, maxPrice, 0);
					[pqPrice, newQualNeed, pqOrder] = getListData(list);
					l("p: " + pqPrice + ", q: " + newQualNeed + ", o: " + pqOrder);
					steps.push({
						cost: required * pqPrice,
						quality: newQualNeed,
					});
					if (newQualNeed > qualNeed)
						qualNeed = newQualNeed;
					qualNeed *= 1.1;
				} while (qualNeed < pqQuality);

				steps.push({
					cost: optCost,
					quality: optQuality,
				});
				s.setItem("auto-unit-apprise-opt-pos", steps.length - 1);

				var stepsUp = [];
				maxPrice = pqPrice * 5; qualNeed = 100;
				do {
					list = makeList(type, pqList, required, qualNeed, maxPrice, 0);
					[pqPrice, newQualNeed, pqOrder] = getListData(list);
					l("p: " + pqPrice + ", q: " + newQualNeed + ", o: " + pqOrder);
					stepsUp.unshift({
						cost: required * pqPrice,
						quality: newQualNeed,
					});
					if (newQualNeed < qualNeed)
						qualNeed = newQualNeed;
					qualNeed *= 0.9;
				} while (qualNeed > pqQuality);
				s.setItem("auto-unit-apprise-steps", JSON.stringify(steps.concat(stepsUp)));
				break;
			default:
		}

		function finish() {
			data_is_changed = false;
			if (s.getItem("auto-supply-create-dont-close") > 0) {
				s.setItem("auto-supply-create-do", "");
				s.setItem("auto-supply-create-done", 1);
				s.setItem("auto-supply-create-dont-close", 0);
			} else {
				s.setItem("auto-supply-create-do", "");
				s.setItem("auto-supply-create-done", 1);
				window.close();
			}
		}

		// $(document).ajaxStop(function() {}) - не работает =(
		if (ajaxClick > 0)
			setTimeout(function () { // костыль =(((
				finish();
			}, 1000 + ajaxClick * 500);
		else
			finish();
	}
}

document.onreadystatechange = function () {
	if (document.readyState === "complete") {
		avRun();
	}
};
document.onreadystatechange();
